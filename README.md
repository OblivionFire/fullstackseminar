# FullStackSeminar

This was the final accumulation of work from a seminar on full stack application development. The code is a simple unit converter, one unit conversion per person that worked on it. From there all people assembled their code into one project.

## Getting Started

The project is a full stack application run out of visual studio. The application runs on a web browser. 

### Prerequisites

Visual Studio Community 2013 or newer, and Google chrome or Firefox

### Installing

Simply clone the repo, load the Visual Studio project, build a solution, and compile.

## Built With

* [Visual Studio 2017](https://www.visualstudio.com/) - Used for GUI design, frameworks, and code development.

## Contributing

## Authors

* **Jacob Patrick** - *Gram to Ounce conversion* - [OblivionFire](https://github.com/OblivionFire)


See also the list of [contributors](https://github.com/OblivionFire/MediMinder/contributors) who participated in this project.

## License

This project is open source for anyone to use. Feel free to build on or update the code, and submit pull requests with the updates.

## Acknowledgments

Seminar run by Ryan Osterberg
